package com.robotico.lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.robotico.listener.ExtentTestNGITestListener;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class SQLConnectionHelper {
	private static final Logger logger = LogManager.getLogger(SQLConnectionHelper.class);
	private static String roboticoDbIp = "";
	private static String roboticoDbPort;
	private static String roboticoEtonDbSchema = "";
	private static String roboticoForthDbSchema = "";
	private static String roboticoBillSchema = "";
	private static String roboticoDbUsername = "";
	private static String roboticoDbPassword = "";

	private static HikariDataSource eton_rei_dataDataSource;
	private static HikariDataSource roboticoDataSource;
	private static HikariDataSource roboticoBillSource;

	private static String AzureDBIP;
	private static String AzureUserName;
	private static String AzurePassword;

	private static HikariDataSource roboticoFrothDataSource;

	static {

		try {
			if (System.getProperty("location").equalsIgnoreCase("local"))
				roboticoDbIp = "10.23.6.31";
			else if (System.getProperty("location").equalsIgnoreCase("localr"))
				roboticoDbIp = "172.31.35.19";
			else if (System.getProperty("location").equalsIgnoreCase("locale"))
				roboticoDbIp = "10.23.6.28";

			roboticoDbPort = "3306";
			if (System.getProperty("location").equalsIgnoreCase("local")
					|| System.getProperty("location").equalsIgnoreCase("localr")) {
				roboticoDbUsername = "anil";
				roboticoDbPassword = "Anil@123";
			} else if (System.getProperty("location").equalsIgnoreCase("locale")) {
				roboticoDbUsername = "tony";
				roboticoDbPassword = "Tony@123";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static HikariDataSource setMySqlConnDS(String ip, String port, String schema, String userName,
			String password, int min, int max) {
		HikariDataSource hikariDataSource = null;

		try {
			logger.info("calling setMySqlConnDS");
			HikariConfig config = new HikariConfig();
			config.setDriverClassName("com.mysql.cj.jdbc.Driver");
			config.setJdbcUrl("jdbc:mysql://" + ip + ":" + port + "/" + schema + "?autoReconnect=true&useSSL=false");
			config.setMinimumIdle(0);
			config.setMaximumPoolSize(100);
			config.setUsername(userName);
			config.setPassword(password);
			config.addDataSourceProperty("cachePrepStmts", "true");
			config.addDataSourceProperty("prepStmtCacheSize", "250");
			config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
			hikariDataSource = new HikariDataSource(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hikariDataSource;

	}

	private static HikariDataSource setAzureSqlServer(String url, String userName, String databaseName, int min,
			int max) {
		HikariConfig config = new HikariConfig();
		HikariDataSource hikariDataSource;
		config.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		System.out.println(url);
		config.setJdbcUrl(
				"jdbc:sqlserver://" + url + ";database=" + databaseName + ";Authentication=ActiveDirectoryInteractive");
		config.setMinimumIdle(0);
		config.setMaximumPoolSize(100);
		config.setUsername(userName);
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		hikariDataSource = new HikariDataSource(config);

		return hikariDataSource;

	}

	public static Connection getEton_rei_dataDBConnBasedOnSites() {

		if (System.getProperty("azure").equalsIgnoreCase("true")) {
			try {
				if (getEton_rei_dataDataSource() == null || getEton_rei_dataDataSource().isClosed()) {
					try {
						AzureDBIP = MainUtil.siteDetails.get("azure_db_server_name");
						AzureUserName = MainUtil.siteDetails.get("azure_db_username");
						setEton_rei_dataDataSource(setAzureSqlServer(AzureDBIP, AzureUserName, "eton_rei_data", 2, 5));
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("There is issue in getting azure eton_rei_data db pool connection");
					}
				}
				logger.info("Connecting to the ODS DB based");
				return getEton_rei_dataDataSource().getConnection();
			} catch (SQLException e) {
				logger.error(MainUtil.ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "getODSDBConnection", e);
				e.printStackTrace();
			}

		}
		return null;
	}


	public static Connection getBillShcemaDBConnection() {
		try {
			roboticoBillSchema = "bill";
			if (roboticoBillSource == null || roboticoBillSource.isClosed()) {
				try {
					roboticoBillSource = setMySqlConnDS(roboticoDbIp, roboticoDbPort, roboticoBillSchema,
							roboticoDbUsername, roboticoDbPassword, 8, 40);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("There is issue in getting getEtonShcemaDBConnection db pool connection");
				}
			}
			logger.info("Getting the bill New database connection from the pool");
			return roboticoBillSource.getConnection();
		} catch (SQLException sqe) {
			logger.error("Error in getting the database connection from the pool\n", sqe);
		}
		return null;
	}

	public static Connection getEtonShcemaDBConnection() {
		try {
			roboticoEtonDbSchema = "eton";
			if (roboticoDataSource == null || roboticoDataSource.isClosed()) {
				try {
					roboticoDataSource = setMySqlConnDS(roboticoDbIp, roboticoDbPort, roboticoEtonDbSchema,
							roboticoDbUsername, roboticoDbPassword, 8, 40);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("There is issue in getting getEtonShcemaDBConnection db pool connection");
				}
			}
			logger.info("Getting the PMAS New database connection from the pool");
			return roboticoDataSource.getConnection();
		} catch (SQLException sqe) {
			logger.error("Error in getting the database connection from the pool\n", sqe);
		}
		return null;
	}

	public static Connection getFrothShcemaDBConnection() {
		try {
			roboticoForthDbSchema = "froth";
			if (roboticoFrothDataSource == null || roboticoFrothDataSource.isClosed()) {
				try {
					roboticoFrothDataSource = setMySqlConnDS(roboticoDbIp, roboticoDbPort, roboticoForthDbSchema,
							roboticoDbUsername, roboticoDbPassword, 8, 40);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("There is issue in getting tmanoDataSource db pool connection");
				}
			}
			logger.info("Getting the PMAS New database connection from the pool");
			return roboticoFrothDataSource.getConnection();
		} catch (SQLException sqe) {
			logger.error("Error in getting the database connection from the pool\n", sqe);
		}
		return null;
	}

	public static void closeDBConnPool() {
		{
			logger.info("Closing the DB connection pool");

			if (roboticoDataSource != null) {
				logger.info("Closing tmanoDataSource");
				roboticoDataSource.close();
			}
		}
	}

	public static void storeintoDB(String steps, String stepResult) {
		try (Connection con = SQLConnectionHelper.getEtonShcemaDBConnection()) {
			String query = "insert into testexecution_details (id,execution_id,scenario,testcase,steps,status,vm_no,siteid) values(uuid(),?,?,?,?,?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, MainUtil.scenarioDetails.get("Scenario"));
				stmt.setString(3, MainUtil.testcaseName);
				stmt.setString(4, steps);
				stmt.setString(5, stepResult);
				stmt.setString(6, MainUtil.vm_no.replace("VM", ""));
				stmt.setString(7, MainUtil.SITE_ID);

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("insertion unsuccessful to the execution table");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void storePMASintoDB(String pagename, double totalPageLoadTime_decimal, String expected_time,
			double totalPageSize_decimal, String status, File fileLocation) {
		try (Connection con = SQLConnectionHelper.getEtonShcemaDBConnection()) {
			String query = "insert into pmas_details (id,execution_id,page_name,actual_time,expected_time,page_size,status,siteid,harfile,harfileLocation) values(uuid(),?,?,?,?,?,?,?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, pagename);
				stmt.setString(3, totalPageLoadTime_decimal + "");
				stmt.setString(4, expected_time);
				stmt.setString(5, totalPageSize_decimal + "");
				stmt.setString(6, status);
				stmt.setString(7, MainUtil.SITE_ID);
				InputStream inputStream = new FileInputStream(fileLocation);
				stmt.setBlob(8, inputStream);
				stmt.setString(9, fileLocation.getPath());

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("insertion unsuccessful to the pmas table");

				// fileLocation.deleteOnExit();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void UpdateToInInExecutionDetails(String status) {
		try (Connection con = SQLConnectionHelper.getEtonShcemaDBConnection()) {
			String query = "update executionDetails set status=? where executionId=? and siteid=? and vm_no=? and status=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, status);
				stmt.setString(2, ExtentTestNGITestListener.executionId);
				stmt.setString(3, MainUtil.SITE_ID);
				stmt.setString(4, MainUtil.vm_no.replace("VM", ""));
				stmt.setString(5, status.equalsIgnoreCase("InProgress") ? "New" : "InProgress");

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Updation unsuccessful to the executionDetails table");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void InsertToExecutionDetails() {
		try (Connection con = SQLConnectionHelper.getEtonShcemaDBConnection()) {
			String query = "insert into  executionDetails(executionId,siteid,vm_no) values(?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, MainUtil.SITE_ID);
				stmt.setString(3, MainUtil.vm_no.replace("VM", ""));

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Insertiong into executionDetails table completed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void InsertToExecutions() {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "insert into  executions(execution_id,comments,status) values(?,?,?)";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, ExtentTestNGITestListener.executionId);
				stmt.setString(2, MainUtil.SITE_ID);
				stmt.setString(3, "j");

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Insertiong into executions table completed");
				else
					System.out.println("Insertiong into executions table failed");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<String> readAllXMLMappingPath() {
		ArrayList<String> testCycleId = null;
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "SELECT ts.`scenario_xml_name` as xml_name FROM `test_scenarios` ts ,`cycle_testscenario` cts,`cycles` c WHERE c.`id`= cts.`cycle_id` AND cts.`test_scenario_id`=ts.id AND c.`formatted_id`=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, System.getProperty("testcycle_name"));
				testCycleId = new ArrayList<String>();
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						testCycleId.add(rs.getString("xml_name"));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return testCycleId;

	}

	public static void updateXMLpath(String XMLPATH) {
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "update cycles set xml_name=? where formatted_id=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, XMLPATH);
				stmt.setString(2, System.getProperty("testcycle_name"));

				boolean rs = stmt.execute();
				if (rs)
					System.out.println("Updation unsuccessful to the executionDetails table");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getModuleName(String sceanrioName) {
		String modulename = "";
		try (Connection con = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String query = "SELECT m.`name`as moduleName FROM `modules` m , `test_scenarios` s WHERE m.id=s.module_id AND s.`testscenario_name`=?";
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				stmt.setString(1, sceanrioName);
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						modulename = rs.getString("moduleName");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return modulename;

	}

	public static HashMap<String, String> readFrmBaseDataBasedOnScenario(String scenario, String tableName) {
		System.out.println("Reached here in readFrmBaseDataBasedOnScenario");

		HashMap<String, String> data = new HashMap<String, String>();

		try (Connection conn = SQLConnectionHelper.getEtonShcemaDBConnection()) {
			String strQuery;
			strQuery = "SELECT * FROM " + tableName + " where Scenario = ? ";

			try (PreparedStatement preparedStatement = conn.prepareStatement(strQuery)) {
				preparedStatement.setString(1, scenario.replaceAll("\\s+", " "));

				try (ResultSet recordset = preparedStatement.executeQuery()) {
					ResultSetMetaData rsmd;
					while (recordset.next()) {
						rsmd = recordset.getMetaData();
						for (int i = 1; i < rsmd.getColumnCount(); i++) {
							data.put(rsmd.getColumnName(i), recordset.getString(i));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public static HashMap<String, String> getSiteDetails() throws Exception {

		HashMap<String, String> data = new HashMap<String, String>();

		try (Connection conn = SQLConnectionHelper.getFrothShcemaDBConnection()) {
			String strQuery;

			strQuery = "SELECT * FROM sites where id=? ";

			try (PreparedStatement preparedStatement = conn.prepareStatement(strQuery)) {
				preparedStatement.setString(1, MainUtil.SITE_ID);

				try (ResultSet recordset = preparedStatement.executeQuery()) {
					ResultSetMetaData rsmd;
					while (recordset.next()) {
						rsmd = recordset.getMetaData();
						for (int i = 1; i < rsmd.getColumnCount(); i++) {
							data.put(rsmd.getColumnName(i), recordset.getString(i));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return data;

	}

	public static HikariDataSource getEton_rei_dataDataSource() {
		return eton_rei_dataDataSource;
	}

	public static void setEton_rei_dataDataSource(HikariDataSource eton_rei_dataDataSource) {
		SQLConnectionHelper.eton_rei_dataDataSource = eton_rei_dataDataSource;
	}

	
	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * 
	 * 
	 * SQLServerDataSource ds = new SQLServerDataSource();
	 * ds.setServerName("sql34tstserverless.database.windows.net"); // Replace with
	 * your server name // ds.setDatabaseName("demo"); // Replace with your database
	 * ds.setAuthentication("ActiveDirectoryInteractive");
	 * 
	 * // Optional login hint ds.setUser("prasad.c@eton-solutions.com"); // Replace
	 * with your user name
	 * 
	 * try (Connection connection = ds.getConnection(); Statement stmt =
	 * connection.createStatement(); ResultSet rs =
	 * stmt.executeQuery("SELECT SUSER_SNAME()")) { if (rs.next()) {
	 * System.out.println("You have successfully logged on as: " + rs.getString(1));
	 * } } catch (Exception e) { e.printStackTrace(); } }
	 */

}
