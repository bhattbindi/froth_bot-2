package com.robotico.lib;

import java.io.IOException;

import com.robotico.base.DriverFactory;

/**
 * Created by Muthulakshmi on 30/3/18.
 */

public abstract class CustomLoadableComponent<T extends CustomLoadableComponent<T>> {

	@SuppressWarnings("unchecked")
	public T get(String name) {
		try {
				isLoaded();
				if (System.getProperty("pmas").equalsIgnoreCase("true"))
					try {
						PageLoad.captureNetwork(name);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				return (T) this;
		} catch (Error e) {
			// This is the extra line of code
			System.out.println("Error encountered during page load: " + e.getMessage());
			if (System.getProperty("pmas").equalsIgnoreCase("true"))
				DriverFactory.getProxy().newHar(name);
			load();
		}
		isLoaded();
//			PageLoad.captureNetwork(name);
		return (T) this;
	}

	protected abstract void load();

	protected abstract void isLoaded() throws Error;
}
