package com.robotico.lib;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationUtil extends MainUtil {

    private static Logger logger = LogManager.getLogger(ApplicationUtil.class);

    /**
     * Method to calculate LhunChecksum for sim serial number
     *
     * @param data pass the 18 digit sim serial no
     * @return the 19 digit sim serial No as a String
     */
    public static synchronized String luhnchecksum(String data) {
        String serialNo = null;
        try {
            logger.info("LUHN check sum");
            serialNo = data;
            String digit;
            /* convert to array of int for simplicity */
            int[] digits = new int[serialNo.length()];
            for (int i = 0; i < serialNo.length(); i++) {
                digits[i] = Character.getNumericValue(serialNo.charAt(i));
            }
            /* double every other starting from right - jumping from 2 in 2 */
            for (int i = digits.length - 1; i >= 0; i -= 2) {
                digits[i] += digits[i];
                if (digits[i] >= 10) {
                    digits[i] = digits[i] - 9;
                }
            }
            int sum = 0;
            for (int i = 0; i < digits.length; i++) {
                sum += digits[i];
            }
            /* multiply by 9 */
            sum = sum * 9;
            /* convert to string to be easier to take the last digit */
            digit = sum + "";
            logger.info("digit is {}", digit.substring(digit.length() - 1));
            String checkDigit = digit.substring(digit.length() - 1);
            logger.info("SIM NO IS {} {}", MainUtil.ProjectConst.VALUE, serialNo.concat(checkDigit));
            // Concatenate the sim serial no with check digit
            serialNo = serialNo.concat(checkDigit);
            storeVariable.put("19DIGIT_SIM_NO", serialNo);
            logger.info("19DIGIT_SIM_NO {}", MainUtil.ProjectConst.VALUE + storeVariable.get("19DIGIT_SIM_NO"));
            //getTest().get().pass("19DIGIT_SIM_NO" + MainUtil.ProjectConst.VALUE + storeVariable.get("19DIGIT_SIM_NO"));
        } catch (Exception e) {
            logger.error(MainUtil.ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "luhnchecksum", e);
            //getTest().get().fail("Error occured in luhnchecksum");
        }
        return serialNo;
    }

    public static String getUUID() {
        String UUID = null;
        try (Connection tmanocon = SQLConnectionHelper.getEtonShcemaDBConnection()) {
            String uuidQuery = "select UUID()";
            try (PreparedStatement preparedStmt = tmanocon.prepareStatement(uuidQuery)) {
                try (ResultSet result = preparedStmt.executeQuery()) {
                    while (result.next()) {
                        UUID = result.getString(1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return UUID;
    }

    public static void insertTestExecutionStatus(Connection conn, String cycle_scenario_table_id, String testcase_name, String exeStatus) {
        String test_case_id = null;
        String query1 = "SELECT tcl.id FROM `tc_cycle_scenario_lists` AS tcsl LEFT JOIN `tc_scenario_test_case_links` AS tstcl ON tstcl.`scenario_id`=tcsl.`scenario_id` LEFT JOIN `tc_test_cases_lists` AS tcl ON tcl.id=tstcl.`test_case_id` WHERE tcsl.`id` =? and tcl.tc_name=?";

        try (PreparedStatement preparedStmt = conn.prepareStatement(query1, Statement.RETURN_GENERATED_KEYS)) {
            preparedStmt.setString(1, cycle_scenario_table_id);
            preparedStmt.setString(2, testcase_name);
            System.out.println(cycle_scenario_table_id);
            System.out.println(testcase_name);
            try (ResultSet rs = preparedStmt.executeQuery()) {
                if (rs.next()) {
                    test_case_id = rs.getString(1);
                    System.out.println("test_case_id: " + test_case_id);
                }
            }
            System.out.println(query1);

        } catch (SQLException sq) {
            sq.printStackTrace();
        }

        String uuid = getUUID();
        MainUtil.storeVariable.put("UUID", uuid);
        String query = "INSERT INTO `tmano`.`tc_cycle_scenario_test_results` (`id`, `cycle_scenario_id`, `test_case_id`, `execution_status`, `remarks`, `created_by`, `created_at`, `updated_at`, `deleted_at`)VALUES(?, ?, ?, ?, 'Starting TC Execution', '1', NOW(), NOW(), NULL);";
        try (PreparedStatement preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStmt.setString(1, uuid);
            preparedStmt.setString(2, cycle_scenario_table_id);
            preparedStmt.setString(3, test_case_id);
            preparedStmt.setString(4, exeStatus);
            System.out.println(query);
            preparedStmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void stepResult(Connection conn, String exeStatus) {
        String query = "UPDATE `tmano`.`tc_cycle_scenario_test_results` SET `execution_status` = ?,`remarks` ='' WHERE `id` = '" + MainUtil.storeVariable.get("UUID") + "'";
        try (PreparedStatement preparedStmt = conn.prepareStatement(query)) {
            preparedStmt.setString(1, exeStatus);
            preparedStmt.executeUpdate();
            System.out.println(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    
    public synchronized void insertStoreVariableIntoList(String userType)
            throws Exception {
        String env = null;
        if(MainUtil.ENVIRONMENT.equalsIgnoreCase("iot")) {
        	env="sit";
        }else if(MainUtil.ENVIRONMENT.equalsIgnoreCase("preprod")) {
        	env="uat";
        }
        try {
            logger.info("Insert store variable into list");
            try (Connection conn = SQLConnectionHelper.getEtonShcemaDBConnection()) {
                String sql = "insert into `store_session_details` (`id`,`user_id`,`user_name`,`user_type`,`password`,`env`,`email_id`,created_at) "
                        + "values (UUID(),?,?,?,?,?,?,NOW())";
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, storeVariable.get("USER_ID") == null ? "NA" : storeVariable.get("USER_ID"));
                    ps.setString(2, storeVariable.get("USERNAME_NEW") == null ? "NA" : storeVariable.get("USERNAME_NEW"));
                    ps.setString(3, storeVariable.get("USER_ROLE") == null ? "NA" : storeVariable.get("USER_ROLE"));
                    ps.setString(4,
                            storeVariable.get("PASSWORD_NEW") == null ? "NA" : storeVariable.get("PASSWORD_NEW"));
                    ps.setString(5,env);
                    ps.setString(6, storeVariable.get("EMAIL") == null ? "NA" : storeVariable.get("EMAIL"));
                    logger.info(sql);
                    boolean rs = ps.execute();
                    if (rs) {
                        logger.info("insrtion Unsuccessful ...");
                    } else {
                        logger.info("insreted successfully ...");
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("ERROR OCCURED WHILE INSERTING INTO THE STORE_SESSION_DETAILS TABLE");
            logger.error(MainUtil.ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "insertStoreVariableIntoList", ex);
        }
    }
 
  public synchronized void FetchStoredDataFromList()
            throws Exception {
        String query = null;
        String userType = storeVariable.get("USER_ROLE");
        String env = null;
        if(MainUtil.ENVIRONMENT.equalsIgnoreCase("iot")) {
        	env="sit";
        }else if(MainUtil.ENVIRONMENT.equalsIgnoreCase("preprod")) {
        	env="uat";
        }
        try (Connection con1 = SQLConnectionHelper.getEtonShcemaDBConnection()) {
            try (Statement stmt1 = con1.createStatement()) {
                query = "SELECT user_id,user_name,password,email_id"
                        + " FROM `store_session_details` WHERE `env`='" + env + "'" + " AND `user_type`='" + userType+"' ORDER BY created_at DESC";
                logger.info(query);
                try (ResultSet rs1 = stmt1.executeQuery(query)) {
                    if (rs1.next()) {
                        storeVariable.put("USER_ID", rs1.getString("user_id"));
                        storeVariable.put("USER_NAME", rs1.getString("user_name"));
                        storeVariable.put("PASSWORD", rs1.getString("password"));
                        storeVariable.put("EMAIL_ID", rs1.getString("email_id"));
                    } else {
                        logger.info("#### There is no record found in DB ");
                    }
                    logger.info(storeVariable.get("USER_ID"));
                    logger.info(storeVariable.get("USER_NAME"));
                    logger.info(storeVariable.get("PASSWORD"));
                    logger.info(storeVariable.get("EMAIL_ID"));
                }
            }
        } catch (Exception e) {
            logger.error("### Some error occured FetchStoredDataFromList");
            logger.error(MainUtil.ProjectConst.EXCEPTIONTEXTMETHOD.getMsg() + " " + "FetchStoredDataFromList", e);
            throw e;
        }
    }

}
